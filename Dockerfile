FROM ubuntu:20.04
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y openjdk-11-jdk maven && \
    apt-get install -y python3-pip && \
    python3 -m pip install ujson && \
    apt-get update && \
    apt-get clean && \
    apt-get autoremove -y
